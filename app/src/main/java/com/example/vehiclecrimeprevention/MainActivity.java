package com.example.vehiclecrimeprevention;

import static com.example.vehiclecrimeprevention.CommonUtil.DEFAULT_SOCKET_TIMEOUT;

import android.net.Uri;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.vehiclecrimeprevention.bean.UserBean;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final int PERMISSIONS_REQUEST_CODE = 10001;
    private AsyncHttpClient client = new AsyncHttpClient();
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager viewPager = findViewById(R.id.viewPager);

        AuthenticationPagerAdapter pagerAdapter = new AuthenticationPagerAdapter(
            getSupportFragmentManager());
        pagerAdapter.addFragmet(new LoginFragment());
        viewPager.setAdapter(pagerAdapter);


    }


    @Override
    protected void onStart() {
        Log.d(TAG, "ON_START....");
        super.onStart();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat
                    .checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "All permission granted");
                } else {
                    Log.d(TAG, "Require ACCESS_COARSE_LOCATION");
                    ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1003);
                    return;
                }
            } else {
                Log.d(TAG, "Require ACCESS_FINE_LOCATION");
                ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1002);
                return;
            }
        } else {
            Log.d(TAG, "Require ACCESS_BACKGROUND_LOCATION");
            ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, 1001);
            return;
        }

    }

    public void openRegisterLink(View view) {
        TextView txt = (TextView) findViewById(R.id.swipeRight);
        txt.setMovementMethod(LinkMovementMethod.getInstance());
        txt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse(CommonUtil.REGISTER_URL));
                startActivity(browserIntent);
            }
        });

    }

    public void login(View view) {

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        EditText tvUserName = (EditText) findViewById(R.id.et_email);
        EditText tvPassword = (EditText) findViewById(R.id.et_password);

        if (tvPassword.getText().toString().trim().length() == 0) {
            progressBar.setVisibility(View.INVISIBLE);
            tvPassword.setError("Please enter your password");
        } else {
            tvPassword.setError(null);
        }

        if (tvUserName.getText().toString().trim().length() == 0) {
            progressBar.setVisibility(View.INVISIBLE);
            tvUserName.setError("Please enter your username (email)");
        } else {
            tvUserName.setError(null);
        }



        client.setTimeout(DEFAULT_SOCKET_TIMEOUT);
        if (tvUserName.getText().toString().trim().length() > 0) {
            if (tvPassword.getText().toString().trim().length() > 0) {
                client
                    .get(
                        CommonUtil.REST_URL + "/validate_login/" + tvUserName.getText().toString()
                            + "/"
                            + tvPassword.getText().toString() + "",
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onStart() {
                                // called before request is started
                                Log.d(TAG, "On start calling service");
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers,
                                byte[] responseBody) {
                                try {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    UserBean userBean = new UserBean();
                                    String json = new String(responseBody);
                                    userBean = (UserBean) CommonUtil
                                        .convertJsonToObject(json, userBean);

                                    Log.d(TAG, "LOGIN RESPONSE " + userBean.toString());
                                    if (userBean != null && userBean.isSuccess()) {
                                        Intent myIntent = new Intent(MainActivity.this,
                                            MapsActivity.class);
                                        myIntent.putExtra(CommonUtil.USER_BEAN, userBean);
                                        MainActivity.this.startActivity(myIntent);
                                        Log.d(TAG, "Access granted");
                                    } else {
                                        Log.d(TAG, "Unable to login");
                                        Toast.makeText(MainActivity.this, "Invalid login details", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Toast.makeText(MainActivity.this, "Error when login in: "+ e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                    Log.d(TAG, "Error when login in: " + e.getLocalizedMessage());
                                }

                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers,
                                byte[] responseBody,
                                Throwable error) {
                                progressBar.setVisibility(View.INVISIBLE);
                                Log.d(TAG, "onFailure calling service. statusCode: " + statusCode);
                                Toast.makeText(MainActivity.this, "Service unavailable", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onRetry(int retryNo) {
                                // called when request is retried
                            }
                        });
            }
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
        @NonNull @org.jetbrains.annotations.NotNull String[] permissions,
        @NonNull @org.jetbrains.annotations.NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d(TAG, "ON_REQUEST_PERMISSIONS_RESULT. Code = " + requestCode);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, " Error, onRequestPermissionsResult: missing permissions");
            ActivityCompat
                .requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_CODE);
            return;
        }

    }


    private void askLocationPermission() {
        Log.d(TAG, "ASK_LOCATION_PERMISSION...");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};
        if (!checkPermission()) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSIONS_REQUEST_CODE);
        } else {
            Log.d(TAG, "ASK_LOCATION_PERMISSION... we have all permissions");
        }
    }

    private boolean checkPermission() {
        return (ContextCompat
            .checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(getApplicationContext(),
            Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(getApplicationContext(),
            Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }
}


class AuthenticationPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> fragmentList = new ArrayList<>();

    public AuthenticationPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    void addFragmet(Fragment fragment) {
        fragmentList.add(fragment);
    }
}