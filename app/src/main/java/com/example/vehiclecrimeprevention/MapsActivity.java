package com.example.vehiclecrimeprevention;

import static com.example.vehiclecrimeprevention.CommonUtil.DEFAULT_SOCKET_TIMEOUT;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.example.vehiclecrimeprevention.bean.HotspotBean;
import com.example.vehiclecrimeprevention.bean.HotspotDetailBean;
import com.example.vehiclecrimeprevention.bean.UserBean;
import com.example.vehiclecrimeprevention.databinding.ActivityMapsBinding;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.CancellationToken;
import com.google.android.gms.tasks.CancellationTokenSource;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import cz.msebera.android.httpclient.Header;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import android.os.Handler;
import org.jetbrains.annotations.NotNull;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MapsActivity";
    private GoogleMap mMap;
    private ActivityMapsBinding binding;
    private GeofencingClient geofencingClient;
    private GeofenceHelper geofenceHelper;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest locationRequest;
    private TextToSpeech textToSpeech;
    private String lastNotificationSentFor = null;
    private boolean exitingNotSent = false;
    AsyncHttpClient client = new AsyncHttpClient();
    HotspotDetailBean hotspotDetailBean = new HotspotDetailBean();
    private UserBean userBean;
    private final Handler handler = new Handler();
    AtomicReference<LatLng> currentLocation = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
            .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        geofencingClient = LocationServices.getGeofencingClient(this);
        geofenceHelper = new GeofenceHelper(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(250);//1000 = 1 seconds
        locationRequest.setFastestInterval(500); //0.5 seconds
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        userBean = (UserBean) getIntent().getSerializableExtra(CommonUtil.USER_BEAN);

        getSupportActionBar().setTitle("Hotspots");


    }


    @Override
    protected void onStart() {
        Log.d(TAG, "ON_START....");
        super.onStart();
        checkSettingsAndStartLocationUpdates();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocationUpdates();
    }


    private void checkSettingsAndStartLocationUpdates() {
        Log.d(TAG, "CHECK_SETTINGS_AND_START_LOCATION_UPDATES");
        LocationSettingsRequest request = new LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest).build();
        SettingsClient client = LocationServices.getSettingsClient(this);

        Task<LocationSettingsResponse> locationSettingsResponseTask = client
            .checkLocationSettings(request);
        locationSettingsResponseTask
            .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                    Log.d(TAG,
                        "checkSettingsAndStartLocationUpdates, onSuccess Settings of device are satisfied and we can start location updates");
                    startLocationUpdates();

                }
            });
        locationSettingsResponseTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG,
                    "checkSettingsAndStartLocationUpdates, onFailure: " + e.getLocalizedMessage());
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException apiException = (ResolvableApiException) e;
                    try {
                        apiException.startResolutionForResult(MapsActivity.this, 1001);
                    } catch (IntentSender.SendIntentException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        fusedLocationClient
            .requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
    }

    private void stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.e(TAG, "ON_MAP_READY...");
        CancellationTokenSource src = new CancellationTokenSource();
        CancellationToken ct = src.getToken();

        if (currentLocation == null) {
            currentLocation = new AtomicReference<>(
                new LatLng(-25.7414047, 28.2094975));

            //Getting current location
            fusedLocationClient.getCurrentLocation(100, ct)
                .addOnSuccessListener(location -> {
                    Log.d(TAG, "Getting current location... Latitude: " + location.getLatitude()
                        + " Longitude: " + location.getLongitude());
                    currentLocation
                        .set(new LatLng(location.getLatitude(), location.getLongitude()));
                })
                .addOnFailureListener(
                    e -> Log.e(TAG, "Get CurrentLocation ERROR: " + e.getMessage()));
        }

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.addMarker(
            new MarkerOptions().position(currentLocation.get()).title("Current Location"));
        CameraUpdate cameraUpdate = CameraUpdateFactory
            .newLatLngZoom(currentLocation.get(), 16);
        mMap.moveCamera(cameraUpdate);

        try {
            mMap.setMyLocationEnabled(true);

            client.setTimeout(DEFAULT_SOCKET_TIMEOUT);
            if (hotspotDetailBean.getHotspotBeanList() == null) {
                String url = CommonUtil.REST_URL + "/find_all_hotspots/" + userBean.getId() + "/"
                    + userBean
                    .getToken() + "";

                Log.d(TAG, "ON MAP READY. URL" + url);
                client
                    .get(url,
                        new AsyncHttpResponseHandler() {

                            @Override
                            public void onStart() {
                                Log.d(TAG, "On start calling service");
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers,
                                byte[] responseBody) {
                                String json = new String(responseBody);
                                Log.d(TAG, "HOTSPOTS JSON RESPONSE: " + json);
                                try {
                                    hotspotDetailBean = (HotspotDetailBean) CommonUtil
                                        .convertJsonToObject(json, hotspotDetailBean);
                                    Log.d(TAG,
                                        "hotspotDetailBean: " + hotspotDetailBean.toString());
                                    addHotspots(currentLocation.get(), hotspotDetailBean);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Log.d(TAG, "Error when calling find all hotspots service: " + e
                                        .getLocalizedMessage());
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers,
                                byte[] responseBody,
                                Throwable error) {
                                String json = new String(responseBody);
                                Log.d(TAG, "onFailure calling service. statusCode: " + statusCode);
                                Log.d(TAG, "onFailure calling service. ERROR: " + json);
                            }

                            @Override
                            public void onRetry(int retryNo) {
                            }
                        });

            } else {
                Log.d(TAG, "USING HOTSPOT_DETAIL_BEAN FROM SESSION");
            }


        } catch (Exception e) {
            e.getStackTrace();
        }

    }


    LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                Log.d(TAG, "onLocationResult, locationResult is null ");
                return;
            }
            for (Location location : locationResult.getLocations()) {
                LatLng currentLocation = new LatLng(location.getLatitude(),
                    location.getLongitude());
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentLocation, 16);
                mMap.moveCamera(cameraUpdate);
                //Updating the mark
                addHotspots(new LatLng(location.getLatitude(), location.getLongitude()),
                    hotspotDetailBean);

            }
        }
    };


    private void addHotspots(LatLng currentLocation, HotspotDetailBean hotspotDetailBean) {

        Log.d(TAG, "Adding hotspots from a service");
        if (hotspotDetailBean != null && hotspotDetailBean.getHotspotBeanList() != null) {

            //Calculating distance
            hotspotDetailBean.getHotspotBeanList().forEach(hotspotDetail -> hotspotDetail
                .setDistance(distance(currentLocation.latitude, hotspotDetail.getLatitude(),
                    currentLocation.longitude, hotspotDetail.getLongitude(), 0.00, 0.00)));

            //Sorting by distance
            Collections.sort(hotspotDetailBean.getHotspotBeanList(),
                (o1, o2) -> o1.getDistance().compareTo(o2.getDistance()));

            List<HotspotBean> hotspotBeanList = hotspotDetailBean.getHotspotBeanList().stream()
                .limit(10)
                .collect(Collectors.toList());//Return the  first 10 hotspots

            Log.d(TAG, "DISTANCE AT 0: " + hotspotBeanList.get(0).getDistance());

            if (ContextCompat
                .checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
                mMap.clear();
                hotspotBeanList.forEach(hotspotDetail -> {
                    LatLng latLng = new LatLng(hotspotDetail.getLatitude(),
                        hotspotDetail.getLongitude());
                    addMarker(latLng, hotspotDetail);
                    addCycle(latLng, Float.parseFloat(hotspotDetailBean.getRadius()));
                    addGeofence(latLng,
                        Float.parseFloat(hotspotDetailBean.getRadius()),
                        UUID.randomUUID().toString());

                    double radius = Double.parseDouble(hotspotDetailBean.getRadius()) + 20;
                    Log.d(TAG, "*********** Radius : " + radius + ", Distance : " + hotspotDetail
                        .getDistance() + " ***********");

                    if ((lastNotificationSentFor == null || !lastNotificationSentFor
                        .equalsIgnoreCase(String.valueOf(hotspotDetail.getId())))
                        && hotspotDetail.getDistance() < radius) {
                        lastNotificationSentFor = String.valueOf(hotspotDetail.getId());
                        exitingNotSent = false;
                        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.bell);
                        mediaPlayer.start();

                        handler.postDelayed(
                            () -> sendVoiceNotification(
                                buildSafetyTips(hotspotDetail.getSafetyTipsList())), 2000);

                    } else if (!exitingNotSent && lastNotificationSentFor != null
                        && lastNotificationSentFor.equalsIgnoreCase(
                        String.valueOf(hotspotDetail.getId()))
                        && hotspotDetail.getDistance() > radius) {
                        exitingNotSent = true;
                        lastNotificationSentFor = null;
                        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.positive);
                        mediaPlayer.start();

                        handler.postDelayed(
                            () -> sendVoiceNotification("Exiting vehicle crime hotspot."), 2000);


                    }


                });
            } else {
                Toast
                    .makeText(this, "NO ACCESS_BACKGROUND_LOCATION PERMISSIONS", Toast.LENGTH_SHORT)
                    .show();
                ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, 1005);
            }
        } else {
            Log.d(TAG, "hotspotDetailBean is null");
        }
    }


    private String buildSafetyTips(List<String> safetyTipsList) {
        StringBuilder sb = new StringBuilder("You are about to enter a vehicle crime hotspot.\n");
        safetyTipsList.forEach(s -> sb.append(s + ".\n"));
        return sb.toString();
    }


    public void sendVoiceNotification(String mssg) {
        textToSpeech = new TextToSpeech(this, status -> {
            if (status == TextToSpeech.SUCCESS) {
                int results = textToSpeech.setLanguage(Locale.ENGLISH);
                if (results == TextToSpeech.LANG_NOT_SUPPORTED || results ==
                    TextToSpeech.LANG_MISSING_DATA) {
                    Log.e(TAG, "Language not supported");
                    Toast.makeText(MapsActivity.this, "Language not supported", Toast.LENGTH_SHORT)
                        .show();
                } else {
                    textToSpeech.setLanguage(Locale.ENGLISH);
                    textToSpeech.setPitch(1);
                    textToSpeech.setSpeechRate((float) 1.5);
                    // NEW LOCATION
                    textToSpeech.speak(mssg, TextToSpeech.QUEUE_FLUSH, null, null);
                }
            } else {
                Log.e(TAG, "Unable to send text to speech message, status is not success");
                Toast.makeText(MapsActivity.this,
                    "Unable to send text to speech message, status is not success",
                    Toast.LENGTH_SHORT).show();
            }
        });
    }


    @SuppressLint("MissingPermission")
    private void addGeofence(LatLng latLng, float radius, String id) {

        Geofence geofence = geofenceHelper.getGeofence(id, latLng, radius,
            Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_DWELL
                | Geofence.GEOFENCE_TRANSITION_EXIT);
        PendingIntent pendingIntent = geofenceHelper.getPendingIntent();

        geofencingClient.removeGeofences(pendingIntent);

        GeofencingRequest geofencingRequest = geofenceHelper.getGeofencingRequest(geofence);

        geofencingClient.addGeofences(geofencingRequest, pendingIntent)
            .addOnSuccessListener(unused -> Log.d(TAG, "onSuccess: geofence added..."))
            .addOnFailureListener(e -> {
                String errorMessage = geofenceHelper.getErrorString(e);
                Log.d(TAG, "onFailure: " + errorMessage);
            });
    }

    private void addMarker(LatLng latLng, HotspotBean hotspotDetail) {
        MarkerOptions markerOptions = new MarkerOptions().position(latLng)
            .title(hotspotDetail.getAdditionalInfo());
        mMap.addMarker(markerOptions);
    }

    private void addCycle(LatLng latLng, float radius) {
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(latLng);
        circleOptions.radius(radius);
        circleOptions.strokeColor(Color.argb(255, 255, 0, 0));
        circleOptions.fillColor(Color.argb(64, 255, 0, 0));
        circleOptions.strokeWidth(4);
        mMap.addCircle(circleOptions);
    }

    /**
     * The haversine formula
     */
    public static double distance(double lat1, double lat2, double lon1, double lon2, double el1,
        double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
            + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);
        double theDistance = Math.sqrt(distance);
        return theDistance;
    }


}