package com.example.vehiclecrimeprevention;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

public class CommonUtil {

    public static final String REST_URL = "http://192.168.255.39:8083/vehicle_crime_prevention/rest";
    public static final String REGISTER_URL = "http://192.168.225.39:8083/vehicle_crime_prevention/register.xhtml";
    public static final String USER_BEAN = "userBean";
    public static final int DEFAULT_SOCKET_TIMEOUT = 30 * 1000; //30 seconds

    public static Object convertJsonToObject(String json, Object objectClass) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, objectClass.getClass());
    }
}
