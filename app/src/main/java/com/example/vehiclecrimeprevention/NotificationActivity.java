package com.example.vehiclecrimeprevention;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

public class NotificationActivity extends AppCompatActivity {

    private static final String TAG = "NotificationActivity";
    private TextToSpeech textToSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        textToSpeech = new TextToSpeech(this, status -> {
            if (status == TextToSpeech.SUCCESS) {
                int results = textToSpeech.setLanguage(Locale.ENGLISH);
                if (results == TextToSpeech.LANG_NOT_SUPPORTED || results ==
                        TextToSpeech.LANG_MISSING_DATA) {
                    Log.e(TAG, "Language not supported");
                    Toast.makeText(NotificationActivity.this, "Language not supported", Toast.LENGTH_SHORT).show();
                } else {

                    textToSpeech.setLanguage(Locale.ENGLISH);
                    textToSpeech.setPitch(1);
                    textToSpeech.setSpeechRate(1);

                    // NEW LOCATION
                    textToSpeech.speak("Hi you succesfully ran me.", TextToSpeech.QUEUE_FLUSH, null, null);

                    Toast.makeText(NotificationActivity.this, "Text to speech initialised", Toast.LENGTH_SHORT).show();
                }
            } else {
                Log.e(TAG, "Unable to send text to speech message, status is not success");
                Toast.makeText(NotificationActivity.this, "Unable to send text to speech message, status is not success", Toast.LENGTH_SHORT).show();
            }
        });

        //TODO STOP SHUTDOWN TTS
        Intent myIntent = new Intent(NotificationActivity.this, MapsActivity.class);
        NotificationActivity.this.startActivity(myIntent);


    }


}